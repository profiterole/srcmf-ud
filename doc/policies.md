Project policies
===================

## Versions and releases

- The version numbers are `upstream-major.upstream-minor.major` where `upstream-major` and `upstream-minor` are specified by the **next** global [UD](universaldependencies.org) release. E.g. if the next UD version is `2.7`, the current SRCMF-UD version is `2.7.x`.
- A new version should be released in two cases
  - A major breaking change such as a significant data addition or a revision of annotation convention that has an impact on a consequent number of trees
  - Small changes such as minor additions and corrections have been done since the last release and a UD data freeze is coming
- During the week just before a UD data freeze, the only merges that should happen are fixes of major errors, improvements and additions should be deferred to the next UD milestone

## Workflow

- `master` should be kept as stable as possible, and guaranteed as much as is practical by automated tests
- All non-trivial changes to `master` should be done by merge request **including those done by maintainers**. The merge request must pass the tests and is approved by a maintainer
  - Motivating changes with references to UD lore and potentially concrete use cases or academic works is very appreciated
- For data changes in particular, the maintainers should strive to obtain the agreement of the other project members, and in particular with linguist experts. Filing an issue for discussion before starting to work on a major change is recommended.
- Merge requests should be kept as atomic as possible in the “feature branch” spirit: if you have several unrelated changes to make, make separate merge requests and merge them sequentially (with rebasing if possible to get a cleaner commit log)

## Project governance

- Changes to these policies can be suggested by filing an issue for discussion
- At least until the end of the ANR Profiterole project, policies changes must be approved by the project leader after discussion with the maintainers.
- At the end of the ANR Profiterole project, a decision must be taken by the project members on the future of the governance.
