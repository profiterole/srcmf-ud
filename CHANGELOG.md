Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## Unreleased

### Changed

- Rename everything to the new `fro-PROFITEROLE-UD` scheme

## 2.13 — 2023-10-31

### Added

- Approximately 1750 sentences imported from the *clari* and *eulali* parts of the original SRCMF,
  which are now our
  [ClariConstantinople](/home/lgrobol/Documents/nlp/dev/srcmf-ud/corpus/ClariConstantinople.conllu)
  and [StEulalie](corpus/StEulalie.conllu).
  - StEulalie ended up entirely in train, accordingly to our policy for very old texts,
    ClariConstantinople has been split randomly between train, dev and test with balancing to keep
    their relative sizes (in number of trees) mostly unchanged, thanks to the new
    [`add_to_split.py`](tools/add_to_split.py) script
- A few scripts to make the process of adding new documents in the future easier.

### Changed

- A few relations have been updated to `nsubj:outer` for conformity with the
  [new](https://universaldependencies.org/changes.html#multiple-subjects) UD policy for multiple
  subject in copular construction with clausal predicates.
- 1773 (in [Roland](corpus/Roland.conllu)) has been updated to better reflect an ellipsis
  construction. Some more fixes of this nature might be coming in future releases.

## 2.9 — 2021-10-29

### Added

- Approximately 350 sentences (mostly averbal) imported from the equivalent texts in [*Base de
  français mediéval*](http://bfm.ens-lyon.fr). Some of them (like interjections) do not have a lot
  of syntactic value, but they make for more consistent and realistic documents.
- The new sentences have been added to the split by attaching them to the part of the previous tree

### Removed

From the sentences in the previous version, we remove:

- 7111 (in Roland), which was actually incomplete
- 11226 (in Graal), which was a duplicate of 11228
- 14607 (in TroyesYvain), which has been merged with 14606

### Changed

- Synchronization with the *Base de français mediéval* : (almost) all tokens now have a `XmlId`
  attribute in `MISC` that link them to the corresponding word in [the TXM
  release](http://txm.ish-lyon.cnrs.fr/bfm/), allowing going back and forth between the two. Some
  token attributes have changed to make them reflect their BFM equivalents
- A total of 2733 punctuation tokens have been added (with automatic heuristic attachments) in texts
  whose transcriptions had punctuation.
- A few sentences from StAlexis that are not in the BFM version have been moved to
  `StAlexis_extra.conllu`.
- A number of manual corrections on existing trees, see the full diff for details.
