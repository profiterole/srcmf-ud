"""Run this as `python tree_ids_from_split.py my_ud_file.conllu` to extract the trees ids."""

import re
import sys

if len(sys.argv) != 2 or sys.argv[1] in ("-h", "--help"):
    print(__doc__)
    sys.exit(0)

with open(sys.argv[1]) as in_stream:
    for l in in_stream:
        m = re.match(r"# sent_id = (?P<id>.*)", l.strip())
        if m:
            print(m.group("id"))

