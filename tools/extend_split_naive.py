from __future__ import annotations

import json
import re

from typing import (
    Dict,
    Iterable,
    List,
    TextIO,
)

import click


@click.command(help="Extend a split by adding new trees to the same part as the previous one.")
@click.argument("split-file", type=click.File("r"))
@click.argument(
    "treebank",
    type=click.Path(
        exists=True,
        dir_okay=False,
        resolve_path=True,
        readable=True,
    ),
    nargs=-1,
)
@click.option(
    "--out",
    "out_file",
    type=click.File(mode="w"),
    default="-",
)
def main(
    split_file: TextIO,
    treebank: Iterable[str],
    out_file: TextIO,
):
    split = json.load(split_file)
    tree_ids: List[str] = []
    for treebank_file in treebank:
        with click.open_file(treebank_file) as in_stream:
            for line in in_stream:
                if m := re.match(r"^# sent_id = (?P<id>[^=\n]+)", line):
                    tree_ids.append(m.group("id"))

    reversed_split: Dict[str, str] = dict()
    for split_name, id_list in split.items():
        reversed_split.update({identifier: split_name for identifier in id_list})

    res: Dict[str, List[str]] = {part: [] for part in split.keys()}
    part = None
    for identifier in tree_ids:
        part = reversed_split.get(identifier, part)
        if part is None:
            raise ValueError(f"Something wrong happened with sentence {identifier}")
        res[part].append(identifier)

    for part_ids in res.values():
        part_ids.sort()

    res = {part: res[part] for part in sorted(res.keys())}

    json.dump(res, out_file, indent=4)

    click.echo("New trees:")
    for part, part_ids in res.items():
        click.echo(f"\t{part}: {len(part_ids)-len(split[part])}")


if __name__ == "__main__":
    main()
