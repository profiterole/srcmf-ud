import sys
from typing import (
    cast,
    Dict,
    Iterable,
    List,
    Literal,
    Optional,
    TextIO,
    Tuple,
)

import conllu
import conllu.models
import click

PAIRS = {
    "(": ")",
    "«": "»",
    "“‘": "’”",
    "“": "”",
    "‘": "’",
}

MAYBE = {",": ","}


def get_punct_type(form: str) -> Literal["opener", "closer", "maybe", "single"]:
    if form in PAIRS.keys():
        return "opener"
    if form in PAIRS.values():
        return "closer"
    if form in MAYBE.keys() or form in MAYBE.values():
        return "maybe"
    return "single"


def enclosed_subtree_root(
    heads: Dict[int, int], a: int, b: int, allow_outgoing: bool = False
) -> Optional[int]:
    # Check for outgoing
    if not allow_outgoing and any(a < h < b and not a < f < b for f, h in heads.items()):
        return None
    incoming_internal = [f for f, h in heads.items() if a < f < b and not a < h < b]
    if not incoming_internal:
        raise ValueError("Non-connex graph")
    # Not a subtree
    elif len(incoming_internal) > 1:
        return None
    else:
        return incoming_internal[0]


def find_paired_punct(
    tree: conllu.TokenList,
) -> Tuple[List[Tuple[conllu.models.Token, conllu.models.Token]], List[conllu.models.Token],]:
    """Find the punctuation in a tree, using UPOS as a filter.

    ## Return

    This returns a couple of lists

    - `paired_punct` a list of paired delimiters as `(opening_token, closing_token)`
    - `other_punct` a list of non-paired punctuation tokens

    ## Notes

    - Matching paired punctuation is no easy feat, we only provide a best-effort heuristic
    - Delimiters that are matched outside of the tree will be in `other_punct`
    """
    id_to_idx = {n["id"]: i for i, n in enumerate(tree)}
    root_idx = id_to_idx[next(t for t in tree if t["head"] == 0 and t["deprel"] == "root")["id"]]
    heads = {
        id_to_idx[n["id"]]: id_to_idx[n["head"]]
        for n in tree
        if n["upos"] != "PUNCT"
        if n["head"] != 0
    }
    paired_punct = []
    other_punct = []
    punct_nodes = tree.filter(upostag="PUNCT", deprel=lambda n: n in ("_", "punct"))
    stack: List[Tuple[Optional[conllu.models.Token], List[conllu.models.Token]]] = [(None, [])]
    for p in punct_nodes:
        p_type = get_punct_type(p["form"])
        if p_type == "opener":
            stack.append((p, []))
        elif p_type == "closer":
            if stack[-1][0] is None:
                other_punct.extend(stack[-1][1])
                stack[-1] = (None, [])
                other_punct.append(p)
            # We do not check if they enclose a subtree here: we wille deal with that later. If they
            # do, we will attach them to its root, if they don't we will crash
            elif PAIRS[stack[-1][0]["form"]] == p["form"]:
                other_punct.extend(stack[-1][1])
                paired_punct.append((stack[-1][0], p))
                stack.pop()
            else:
                raise ValueError(f"Unbalanced delimiters at {p['id']} in {tree.serialize()}.")
        elif p_type == "maybe":
            for i, n in reversed(list(enumerate(stack[-1][1]))):
                if MAYBE[n["form"]] == p["form"]:
                    if (
                        h := enclosed_subtree_root(
                            {**heads, root_idx: -1},
                            id_to_idx[n["id"]],
                            id_to_idx[p["id"]],
                        )
                    ) is not None:
                        if tree[h]["deprel"].split(":")[0] != "conj":
                            other_punct.extend(stack[-1][1][i + 1 :])
                            paired_punct.append((n, p))
                            stack[-1] = (stack[-1][0], stack[-1][1][:i])
                            break
            else:
                stack[-1][1].append(p)
        elif p_type == "single":
            other_punct.append(p)
        else:
            raise ValueError(f"Unknown PUNCT type {p_type!r}")
    # Empty the stack
    for p, o in stack:  # type: ignore[assignment]
        if p is not None:
            other_punct.append(p)
        other_punct.extend(o)
    return paired_punct, other_punct


def get_depth(node_idx: int, heads: Dict[int, int]):
    res = 0
    while (head_idx := heads.get(node_idx)) is not None:
        res += 1
        if res > len(heads):
            raise InvalidTreeError(f"Cyclic dependencies.")
        node_idx = head_idx
    return res


def get_highest_proj(heads: Dict[int, int], node_idx: int, root_idx: int) -> int:
    candidates = set(heads.keys())
    if not candidates:
        return root_idx
    candidates.add(root_idx)
    min_idx = 0
    max_idx = max(node_idx, *candidates)
    for dep, gov in (
        (root_idx, -1),
        (root_idx, max_idx + 1),
        *heads.items(),
    ):
        if not min(dep, gov) < node_idx < max(dep, gov):
            candidates.difference_update(range(min(dep, gov) + 1, max(dep, gov)))
        else:
            candidates.difference_update(range(min_idx, min(dep, gov)))
            candidates.difference_update(range(max(dep, gov) + 1, max_idx + 1))
            min_idx = min(dep, gov)
            max_idx = max(dep, gov)
        if len(candidates) == 1:
            return next(iter(candidates))
    return min(candidates, key=lambda n: (get_depth(n, heads), n))


def crossing(rel1: Tuple[int, int], rel2: Tuple[int, int]) -> bool:
    (a, b), (c, d) = sorted((sorted(rel1), sorted(rel2)))
    return a < c < b < d


class InvalidTreeError(Exception):
    pass


def attach_punct_(tree: conllu.TokenList):
    """Attach the punctuation in a given tree in place

    We follow the [UD guidelines](https://universaldependencies.org/u/dep/punct)

    1. A punctuation mark separating coordinated units is attached to the following conjunct.
    2. A punctuation mark preceding or following a dependent unit is attached to that unit.
    3. Within the relevant unit, a punctuation mark is attached at the highest possible node that
       preserves projectivity.
    4. Paired punctuation marks (e.g. quotes and brackets, sometimes also dashes, commas and other)
       should be attached to the same word unless that would create non-projectivity. This word is
       usually the head of the phrase enclosed in the paired punctuation.

    **NOTE**: when these rules are in conflict, we first try to apply the paired rule
    """
    try:
        root = next(t for t in tree if t["head"] == 0 and t["deprel"] == "root")
    except StopIteration:
        raise InvalidTreeError("Tree with no root") from None
    id_to_idx = {n["id"]: i for i, n in enumerate(tree)}
    try:
        heads = {
            id_to_idx[n["id"]]: id_to_idx[n["head"]]
            for n in tree
            if n["upos"] != "PUNCT" and n["head"] != 0
        }
    except KeyError as e:
        raise InvalidTreeError("Wrong head: there is no node with that id.") from e
    paired_punct, other_punct = find_paired_punct(tree)
    for o, c in paired_punct:
        o_idx = id_to_idx[o["id"]]
        c_idx = id_to_idx[c["id"]]
        pair_head_idx = enclosed_subtree_root(
            {**heads, id_to_idx[root["id"]]: -1},
            o_idx,
            c_idx,
            allow_outgoing=False,
        )
        if pair_head_idx is None or any(
            crossing((o_idx, pair_head_idx), rel) or crossing((pair_head_idx, c_idx), rel)
            for rel in heads.items()
        ):
            # This shouldn't happen, but in case it does, (when attaching the same token would
            # create non-projectivity) we revert to the default mode
            print(
                (
                    f"Inconsistent pairing for nodes {o['id']} and {c['id']}"
                    f" in tree {tree.metadata['sent_id']}:"
                    f" they should attach to node {pair_head_idx} but that would be non-projective."
                )
                ,
                file=sys.stderr,
            )
            other_punct.extend((o, c))
            continue
        pair_head_id = tree[pair_head_idx]["id"]
        o["head"] = pair_head_id
        c["head"] = pair_head_id
        o["deprel"] = "punct"
        c["deprel"] = "punct"
        heads[id_to_idx[o["id"]]] = id_to_idx[o["head"]]
        heads[id_to_idx[c["id"]]] = id_to_idx[c["head"]]
    for p in other_punct:
        p_idx = id_to_idx[p["id"]]
        # First try the special case of coordinations
        conj_head = min(
            (
                id_to_idx[dep["id"]]
                for dep in tree
                if dep["deprel"] == "conj" and id_to_idx[dep["head"]] < p_idx < id_to_idx[dep["id"]]
            ),
            default=None,
        )
        if conj_head is not None and not any(
            crossing((p_idx, conj_head), rel) for rel in heads.items()
        ):
            p_head_idx = conj_head
        else:
            try:
                p_head_idx = get_highest_proj(heads, p_idx, root_idx=id_to_idx[root["id"]])
            except InvalidTreeError as e:
                raise InvalidTreeError(f"Issue with node {p['id']}: {e}") from e
        p["head"] = tree[p_head_idx]["id"]
        p["deprel"] = "punct"
        heads[id_to_idx[p["id"]]] = p_head_idx


@click.command(help="Attach punctuation in a CoNNL-U file following the UD guidelines")
@click.argument(
    "input_file",
    type=click.File("r"),
)
@click.argument(
    "output_file",
    type=click.File("w", atomic=True),
    default="-",
)
def main(input_file: TextIO, output_file: TextIO):
    for tree in conllu.parse_incr(input_file):
        try:
            attach_punct_(tree)
        except InvalidTreeError as e:
            raise click.ClickException(f"Invalid tree {tree.metadata['sent_id']}: {e}") from e
        output_file.write(tree.serialize())


if __name__ == "__main__":
    main()
