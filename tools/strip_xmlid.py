import sys
from typing import Optional, TextIO

import conllu
import conllu.models
import conllu.parser
import click


class InvalidTreeError(Exception):
    def __init__(self, message: str, token_id: Optional[conllu.parser._IdType] = None):
        self.message = message
        self.token_id = token_id


def strip_xmlid_(tree: conllu.TokenList):
    for t in tree:
        if t["misc"]:
            t["misc"] = {k: t["misc"][k] for k in t["misc"].keys() if k != "XmlId"}
        if not t["misc"]:
            t["misc"] = None


@click.command(
    help="Remove XmlId from the misc column"
)
@click.argument(
    "input_file",
    type=click.File("r"),
)
@click.argument(
    "output_file",
    type=click.File("w", atomic=True),
    default="-",
)
def main(input_file: TextIO, output_file: TextIO):
    for tree in conllu.parse_incr(input_file):
        try:
            strip_xmlid_(tree)
        except InvalidTreeError as e:
            print(
                f"Invalid tree: {tree.metadata['sent_id']} ({e.message} at token {e.token_id})",
                file=sys.stderr,
            )

        output_file.write(tree.serialize())


if __name__ == "__main__":
    main()
