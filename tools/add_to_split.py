from collections import Counter

import json
import random
from typing import (
    Sequence,
    TextIO,
)

import click
import conllu
import natsort


@click.command(
    help=(
        "Add new trees to a data split. If several parts are specified, we'll attempt to maintain"
        " their relative sizes (in number of sentences)"
    )
)
@click.argument("split-file", type=click.File("r"))
@click.argument("part_name", nargs=-1)
@click.argument("treebank", type=click.File("r"))
@click.option(
    "--out",
    "out_file",
    type=click.File(mode="w", atomic=True),
    default="-",
)
def main(
    out_file: TextIO,
    part_name: Sequence[str],
    split_file: TextIO,
    treebank: TextIO,
):
    split = json.load(split_file)
    if (part := next((n for n in part_name if n not in split), None)) is not None:
        raise click.ClickException(
            f"Part {part} does not exist in split. Existing parts are: {', '.join(split.keys())}"
        )
    part_sizes = [len(split[n]) for n in part_name]

    new_ids: list[str] = []
    for tree in conllu.parse_incr(treebank):
        if "sent_id" not in tree.metadata:
            raise ValueError(f"Missing sent_id for tree {tree}")
        new_ids.append(tree.metadata["sent_id"])

    # Checking uniqueness
    unique_ids = set(new_ids)
    if len(unique_ids) != len(new_ids):
        count = Counter(new_ids)
        dupes = [i for i, v in count.items() if v > 1]
        raise click.ClickException(f"Duplicated new ids: {dupes}")

    affectations = random.choices(part_name, weights=part_sizes, k=len(new_ids))  # noqa: S311
    for i, n in zip(new_ids, affectations, strict=True):
        split[n].append(i)

    for n in part_name:
        split[n] = natsort.natsorted(split[n])

    json.dump(split, out_file, indent=4)


if __name__ == "__main__":
    main()
