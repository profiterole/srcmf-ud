import pathlib
import re
from typing import (
    Iterable,
    TextIO,
    Tuple,
)

import conllu
import click


def detokenize(tokens: Iterable[str]) -> str:
    res: list[str] = []
    for t in tokens:
        if t in {".", ","} and len(res) > 0:
            if res[-1] == " ":
                res.pop()
        res.append(t)
        if t[-1] not in {"'"}:
            res.append(" ")
    return "".join(res).rstrip()


@click.command(help="Add metadata to the trees in a conllu file")
@click.argument(
    "input_file",
    type=click.File("r"),
)
@click.argument(
    "output_file",
    type=click.File("w", atomic=True),
    default="-",
)
@click.option(
    "--corpus-path",
    help="A corpus directory to ensure that the sent ids are non-overlapping.",
    type=click.Path(
        file_okay=False,
        dir_okay=True,
        readable=True,
        resolve_path=True,
        path_type=pathlib.Path,
    ),
)
@click.option(
    "--base-id",
    help=(
        "A base name for sent_ids. If given, they will be of the form 'BASE_ID-xxx' where xxx is a"
        " number ; if not, they will simply be numeric."
    ),
    metavar="BASE_ID",
)
@click.option(
    "--extra",
    help="A common mextadata line to add to every tree",
    metavar="KEY VALUE",
    multiple=True,
    type=(str, str),
)
def main(
    base_id: str | None,
    corpus_path: pathlib.Path | None,
    extra: Iterable[Tuple[str, str]],
    input_file: TextIO,
    output_file: TextIO,
):
    extra_dict = dict(extra)
    existing_ids: set[str] = set()
    if corpus_path is not None:
        for f in corpus_path.glob("*.conllu"):
            with f.open() as in_stream:
                for tree in conllu.parse_incr(in_stream):
                    if "sent_id" in tree.metadata:
                        existing_ids.add(tree.metadata["sent_id"])
    if base_id is None:
        start_id = max((int(i) for i in existing_ids if i.isnumeric()), default=-1) + 1
        id_pattern = "{i}"
    else:
        # 😘
        start_id = (
            max(
                (int(m.group(1)) for i in existing_ids if (m := re.match(rf"{base_id}-(\d+)", i))),
                default=-1,
            )
            + 1
        )
        id_pattern = f"{base_id}-{{i}}"
    for i, tree in enumerate(conllu.parse_incr(input_file), start=start_id):
        tree.metadata["text"] = detokenize([t["form"] for t in tree])
        tree.metadata["sent_id"] = id_pattern.format(i=i)
        tree.metadata.update(extra_dict)
        output_file.write(tree.serialize())


if __name__ == "__main__":
    main()
