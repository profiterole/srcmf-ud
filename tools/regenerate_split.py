from __future__ import annotations

import dataclasses
import json
import pathlib
import re

from collections import OrderedDict
from typing import (
    Dict,
    Iterable,
    List,
    TextIO,
    cast,
)

import click


METADATA_RE = re.compile(r"^# (?P<key>[^=]+) = (?P<value>[^=]+)$")


@dataclasses.dataclass(eq=False)
class Tree:
    metadata: OrderedDict[str, str]
    nodes: OrderedDict[str, List[str]]

    def to_conll(self) -> str:
        res = []
        for k, v in self.metadata.items():
            res.append(f"# {k} = {v}")
        for node in self.nodes.values():
            res.append("\t".join(node))
        return "\n".join(res)

    @classmethod
    def from_conll(cls, lines: Iterable[str]) -> Tree:
        metadata: OrderedDict[str, str] = OrderedDict()
        nodes: OrderedDict[str, List[str]] = OrderedDict()
        for l in lines:
            if l.startswith("#"):
                m = METADATA_RE.match(l.strip())
                if m:
                    key = m.group("key")
                    if key in metadata:
                        raise ValueError(f"Duplicated metadata entry: {key}")
                    metadata[key] = m.group("value")
            else:
                l_lst = l.strip().split("\t")
                token_id = l_lst[0]
                if token_id in nodes:
                    raise ValueError(f"Duplicated token id: {token_id}")
                nodes[token_id] = l_lst
        return cls(metadata=metadata, nodes=nodes)

    @classmethod
    def load_treebank(cls, f: TextIO) -> OrderedDict[str, Tree]:
        res: OrderedDict[str, Tree] = OrderedDict()
        buffer: List[str] = []
        for line in f:
            if line.isspace():
                if buffer:
                    tree = cls.from_conll(buffer)
                    if tree.nodes:
                        res[tree.metadata["sent_id"]] = tree
                    buffer = []
                else:
                    continue
            else:
                if line.startswith("#") and not METADATA_RE.match(line):
                    continue
                else:
                    buffer.append(line)
        if buffer:
            tree = cls.from_conll(buffer)
            if not tree.nodes:
                raise ValueError("Empty tree")
            sent_id = tree.metadata["sent_id"]
            if sent_id in res:
                raise ValueError(f"Duplicated id: {sent_id!r}")
            res[sent_id] = tree
        return res

    @classmethod
    def dump_treebank(cls, f: TextIO, trees: Iterable[Tree]):
        for t in trees:
            f.write(t.to_conll())
            f.write("\n\n")


@click.command(help="Generate a split from a split file and a treebank.")
@click.argument(
    "split-file",
    type=click.File("r"),
)
@click.argument(
    "treebank",
    type=click.Path(
        exists=True,
        dir_okay=False,
        resolve_path=True,
        readable=True,
    ),
    nargs=-1,
)
@click.option(
    "--out-dir",
    type=click.Path(resolve_path=True, dir_okay=True, path_type=pathlib.Path, writable=True),
    default=pathlib.Path("."),
)
@click.option("--overwrite", is_flag=True)
def main(
    split_file: TextIO,
    treebank: Iterable[str],
    out_dir: pathlib.Path,
    overwrite: bool,
):
    split = json.load(split_file)
    out_dir.mkdir(parents=True, exist_ok=True)
    treebank_contents: Dict[str, Tree] = dict()
    for treebank_file in treebank:
        with click.open_file(treebank_file, "r") as in_stream:
            file_contents = Tree.load_treebank(cast(TextIO, in_stream))
            common_ids = set(file_contents).intersection(treebank_contents)
            if common_ids:
                raise ValueError(f"Duplicated keys: {common_ids}")
            treebank_contents.update(file_contents)


    missing = []
    for split_name, id_list in split.items():
        res_path = out_dir / f"fro-profiterole-ud-{split_name}.conllu"
        if not overwrite and res_path.exists():
            raise ValueError(f"{res_path} already exists")
        with res_path.open("w") as out_stream:
            split_trees = []
            for k in id_list:
                if (t := treebank_contents.get(k)) is None:
                    missing.append(k)
                else:
                    split_trees.append(t)
            Tree.dump_treebank(out_stream, split_trees)

    if missing:
        raise ValueError(f"{len(missing)} trees missing in treebank: {missing}")


if __name__ == "__main__":
    main()
