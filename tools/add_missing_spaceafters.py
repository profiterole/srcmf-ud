from itertools import islice
import re
import sys
from typing import List, Optional, TextIO, Tuple, cast

import conllu
import conllu.models
import conllu.parser
import click


class InvalidTreeError(Exception):
    def __init__(self, message: str, token_id: Optional[conllu.parser._IdType] = None):
        self.message = message
        self.token_id = token_id


def get_tokens(tree: conllu.TokenList) -> List[conllu.models.Token]:
    res = []
    tree_it = iter(tree)
    for token in tree_it:
        res.append(token)
        if isinstance(token["id"], tuple):
            start, *_, end = token["id"]
            for subword in islice(tree_it, end - start + 1):
                if not (start <= subword["id"] <= end):
                    raise InvalidTreeError(
                        f"Wrong range: {token['id']}",
                        token_id=cast(Tuple[int, str, int], token["id"]),
                    )
    return res


def add_missing_spaceafters_(tree: conllu.TokenList):
    tokens = get_tokens(tree)
    text = tree.metadata["text"]
    cur = 0
    for t in tokens[:-1]:
        if t["misc"] is None:
            t["misc"] = {}
        # This because MyPy is buggy
        assert t["misc"] is not None  # noqa: S101
        if not text[cur:].startswith(t["form"]):
            raise InvalidTreeError("Text out of sync", token_id=t["id"])
        cur += len(t["form"])
        m = re.match(r"\s+", text[cur:])
        if not m:
            if t["misc"].get("SpaceAfter") != "No":
                print(
                    f"Added SpaceAfter in tree {tree.metadata['sent_id']} at token {t['id']}",
                    file=sys.stderr,
                )
                t["misc"]["SpaceAfter"] = "No"
        else:
            if t["misc"].get("SpaceAfter") == "No":
                raise InvalidTreeError("Wrong SpaceAfter=No", token_id=t["id"])
            cur += len(m.group(0))
    if tokens[-1]["misc"].get("SpaceAfter") == "No":
        raise InvalidTreeError("Wrong SpaceAfter=No", token_id=tokens[-1]["id"])


@click.command(
    help="Fix missing SpaceAfter misc features from text metadata in CoNLL-U files"
)
@click.argument(
    "input_file",
    type=click.File("r"),
)
@click.argument(
    "output_file",
    type=click.File("w", atomic=True),
    default="-",
)
def main(input_file: TextIO, output_file: TextIO):
    for tree in conllu.parse_incr(input_file):
        try:
            add_missing_spaceafters_(tree)
        except InvalidTreeError as e:
            raise click.ClickException(
                f"Invalid tree: {tree.metadata['sent_id']} ({e.message} at token {e.token_id})",
            ) from e
        output_file.write(tree.serialize())


if __name__ == "__main__":
    main()
