from collections import defaultdict
import json
import pathlib
from typing import DefaultDict, Dict, List, Optional, Tuple

import conllu
import conllu.models
import conllu.parser
import click


class InvalidTreeError(Exception):
    def __init__(self, message: str, token_id: Optional[conllu.parser._IdType] = None):
        self.message = message
        self.token_id = token_id


@click.command(help="Count statistics")
@click.argument(
    "corpus_path",
    type=click.Path(
        file_okay=False,
        dir_okay=True,
        readable=True,
        resolve_path=True,
        path_type=pathlib.Path,
    ),
)
def main(corpus_path: pathlib.Path):
    trees: Dict[str, Tuple[str, conllu.TokenList]] = dict()
    print("Per file:")
    print("file\ttokens\ttrees")
    total_tokens = 0
    total_trees = 0
    for f in corpus_path.glob("*.conllu"):
        with open(f) as in_stream:
            file_trees = list(conllu.parse_incr(in_stream))
            trees.update((t.metadata["sent_id"], (f.stem, t)) for t in file_trees)
            n_tokens = sum(len(t) for t in file_trees)
            n_trees = len(file_trees)
        total_tokens += n_tokens
        total_trees += n_trees
        print(f"{f.stem}\t{n_tokens}\t{n_trees}")
    print(f"total\t{total_tokens}\t{total_trees}")

    print()

    print("Split:")
    with open(corpus_path / "split.json") as in_stream:
        split: Dict[str, List[str]] = json.load(in_stream)
    per_file_split: DefaultDict[str, Dict[str, int]] = defaultdict(
        lambda: {k: 0 for k in split.keys()}
    )
    per_file_split_trees: DefaultDict[str, Dict[str, int]] = defaultdict(
        lambda: {k: 0 for k in split.keys()}
    )
    split_total: Dict[str, int] = {k: 0 for k in split.keys()}
    split_total_trees: Dict[str, int] = {k: 0 for k in split.keys()}
    for partition, trees_ids in split.items():
        for tid in trees_ids:
            f_name, t = trees[tid]
            per_file_split[f_name][partition] += len(t)
            per_file_split_trees[f_name][partition] += 1
            split_total[partition] += len(t)
            split_total_trees[partition] += 1
    # reversed so if it's train test dev we get them in that order
    parts = sorted(split.keys(), reverse=True)

    print("Tokens repartition:")
    print("\t".join(["file", *parts]))
    for f_name, counts in per_file_split.items():
        print("\t".join([f_name, *(str(counts[p]) for p in parts)]))
    print("\t".join(["total", *(str(split_total[p]) for p in parts)]))

    print()

    print("Trees repartition:")
    print("\t".join(["file", *parts]))
    for f_name, counts in per_file_split_trees.items():
        print("\t".join([f_name, *(str(counts[p]) for p in parts)]))
    print("\t".join(["total", *(str(split_total_trees[p]) for p in parts)]))


if __name__ == "__main__":
    main()
