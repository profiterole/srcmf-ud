fro-PROFITEROLE-UD
==================

## Release process

- Clone this repository
- Create a new branch
- Make your changes, commit, push
- Open a merge request on GitLab, this will trigger the UD annotation checking pipeline
- Once your changes pass the test, merge to master

After some iterations of this, you will want to release it to UD proper:

- Create a new release on GitLab
- This will trigger the pipeline, yielding an archive with the UD split
- Add the new version to the Github repo for UD
  - You will need commit permission for that, ask Dan Zeman for them
  - Ideally, only active maintainers should have these permissions
